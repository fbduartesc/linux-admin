# Curso Linux Admin

## Day 4

## Curingas e caracteres especiais

```sh
user@linux-admin:~$ cd /usr/bin
```

Tipos de curingas:

- `*` - identifica nenhum, 1 ou mais caracter.
- `?` - identifica apenas 1 caracter.
- `[]` - um padrão, identificado por colchetes.
- `{}` - um padrão de strings, identificado por chaves.
- `^` - circunflexo, caracter de negação

```sh
user@linux-admin:~$ ls a*
user@linux-admin:~$ ls b*
user@linux-admin:~$ ls *path
user@linux-admin:~$ ls a*z
user@linux-admin:~$ ls a*r
user@linux-admin:~$ ls m?
user@linux-admin:~$ ls m??
```

Podemos combinar curingas.

```sh
user@linux-admin:~$ ls a?t*
user@linux-admin:~$ ls ?z*
user@linux-admin:~$ ls m[a-c]
Todos dentro de um intervalo
user@linux-admin:~$ ls m[^abc]
Comeca com m e não termina com abc
user@linux-admin:~$ ls m[^t-v]
user@linux-admin:~$ ls x{zd,ze}*
Tem um detalhe, se não encontrar mostra mensagem apenas desse caracter que não encontrou.
user@linux-admin:~$ ls w?[d-z]*{list,comp}*
Com a lista ele força duas vezes a pesquisa
user@linux-admin:~$ ls w?[d-z]*list*comp*
```

## Os curingas podem ser usados para todos os comandos do sistema

# Comandos diversos

- `clear` - limpa a tela
- `#` - é um comentário. Um jeito de salvar um comando sem o comando na shell ser executado.

Data em UTC

```sh
user@linux-admin:~$ date -u
```

Para alterar a data e hora.

```sh
user@linux-admin:~$ date 101007452020
```

- `10` - mês
- `10` - dia
- `07` - hora
- `45` - minuto
- `2020` - ano

Como salvar a data na placa-mãe, no sistema.

```sh
user@linux-admin:~$ hwclock --systohc
```

--systohc:

- `h` - hardware
- `c` - clock

```sh
user@linux-admin:~$ date +%d
user@linux-admin:~$ date +%d -%m -%m -%Y
user@linux-admin:~$ date +"%d-%m-%Y %T"
user@linux-admin:~$ date +"%d %Y %j"
user@linux-admin:~$ date --date="@1234567890"
user@linux-admin:~$ date -u --date="@1"
```

Parâmetros de comandos acima:

- `%d` - mostra apenas o dia
- `%d -%m -%m -%Y` - 10-10-2020
- `"%d-%m-%Y %T"` - 10-10-2020 07:52:10
- `"%d %Y %j"` - o dia do ano
- `--date="@1234567890"` - unix time

Informações de disco

```sh
user@linux-admin:~$ df
user@linux-admin:~$ df -h
user@linux-admin:~$ df -H
user@linux-admin:~$ df -l
user@linux-admin:~$ df -m
user@linux-admin:~$ df -a
user@linux-admin:~$ df -i
user@linux-admin:~$ df -T
user@linux-admin:~$ df -T -t ext4
user@linux-admin:~$ df -Th -t tmps
user@linux-admin:~$ df -P
```

Parâmetros de comandos acima:

- `df` - mostra tamanho do disco
- `df -h` - mostra informação mais humana
- `df -H` - mostra os dados em 1000 (mostra o tamanho de venda do HD)
- `df -l` - mostra apenas o sistemas de arquivos locais
- `df -m` - mostra saída em megabytes
- `df -a` - inclui pseudo filesysyem, semelhando ao `mount`
- `df -i` - `inodes`, referente a quantidade de arquivos e pastas que foram criados.
- `df -T` - mostra o sistema de arquivos por partição.
- `df -T -t ext4` - mostra apenas o sistema de arquivos especificado.
- `df -Th -t tmps` - mostra sistema de arquivo tmps
- `df -P` - saída em formato `POSIX`

## ln

- comando que cria um link para um arquivo ou diretório.
- precisa ser root
- `hard link` podem ser feitos dentro do mesmo sistema de arquivos, o resultado como se fosse um arquivo convencional.
- `link simbólico`. Pode criar entre sistemas de arquivos diferentes.

Cria um hard link.
O arquivo é referenciado por inode no sistema de arquivos

```sh
user@linux-admin:~$ ln /bin/ls ls
```

Cria link simbólico.

```sh
user@linux-admin:~$ ln -s /bin/ls ls-link
```

Não é possível criar hard link para diretórios.

```sh
user@linux-admin:~$ cp -rvl /usr /tmp/copia-usr/
user@linux-admin:~$ df -h
user@linux-admin:~$ du -hs
user@linux-admin:~$ df -h
user@linux-admin:~$ df -i
```

## du

- exibe a ocupação do tamanho de cada arquivo
- `-H` - exibe em formato humano, com blocos de 1000.
- `-h` - exibe em formato humano, com blocos de 1024kb.
- `-hs` - vai sumarizar, dar o total.
- `-k` - para mostrar em kb.
- `--inodes` - para mostrar os dados em inodes.
- `-hc` - no final da sumarização quem temos a opção `s`.

```sh
user@linux-admin:~$ du
user@linux-admin:~$ du -H
user@linux-admin:~$ du -h
user@linux-admin:~$ du -hs
user@linux-admin:~$ du -k
user@linux-admin:~$ du --inodes
user@linux-admin:~$ du -hc
```

## find

Localiza arquivos e diretórios. É bastante poderoso.

```sh
user@linux-admin:~$ find
user@linux-admin:~$ find . -name ls
user@linux-admin:~$ find . -type d - name mc
user@linux-admin:~$ find . -type f -name mc
user@linux-admin:~$ find . maxdepth 2 -type f -name mc
user@linux-admin:~$ find . mindepth 3 -maxdepth 4 -type f -name mc
user@linux-admin:~$ find /etc -mtime -1
user@linux-admin:~$ find /etc -amin -10
user@linux-admin:~$ find /etc -cmin -10
user@linux-admin:~$ find /usr -mount -name ls
user@linux-admin:~$ find /usr -gid 1000
user@linux-admin:~$ find /usr -uid 1000
user@linux-admin:~$ find /home -user fabio
user@linux-admin:~$ find /home -group fabio
user@linux-admin:~$ find /usr -links 3
user@linux-admin:~$ find /usr -size -1000
user@linux-admin:~$ find /usr -size -1000k
user@linux-admin:~$ find /usr -size -1000c
user@linux-admin:~$ find /usr -type b
user@linux-admin:~$ find /usr -type p
user@linux-admin:~$ find /dev -type c
user@linux-admin:~$ find /dev -type s
user@linux-admin:~$ find /dev -type l
```

- `find`
- `find . -name ls`
- `find . -type d - name mc` - busca apenas diretórios.
- `find . -type f -name mc` - busca apenas arquivos.
- `find . maxdepth 2 -type f -name mc` - busca em apenas 2 níveis de diretórios.
- `find . mindepth 3 -maxdepth 4 -type f -name mc` - busca com mínimo e máximo.
- `find /etc -mtime -1` - todos os arquivos que foram modificados.
- `find /etc -amin -10` - busca nos últimos 10 minutos.
- `find /etc -cmin -10` - busca arquivos criados nos últimos 10 minutos.
- `find /usr -mount -name ls` - faz a busca apenas nos pontos montados.
- `find /usr -gid 1000` - busca por grupos
- `find /usr -uid 1000` - id do usuário
- `find /home -user fabio` - todos arquivos e diretórios que tem como dono
- `find /home -group fabio` - busca pelo grupo
- `find /usr -links 3` - busca todos os arquivos que tem links como referências.
- `find /usr -size -1000` - busca pelo tamanho do arquivo.
- `find /usr -size -1000k` - busca kb
- `find /usr -size -1000c` - busca por bytes
- `find /usr -type b` - busca por blocos
- `find /usr -type p` - busca por pipe
- `find /dev -type c` - busca por caracter
- `find /dev -type s` - busca por sockets
- `find /dev -type l` - link simbólicos

## free

Mostra memória ram e memória swap. Usa os parâmetros encontrados em `cat /proc/meminfo`.

```sh
user@linux-admin:~$ free
user@linux-admin:~$ free --kilo
user@linux-admin:~$ free --mega
user@linux-admin:~$ free --kibi
user@linux-admin:~$ free --mebi
user@linux-admin:~$ free --gibi
user@linux-admin:~$ free --giga
user@linux-admin:~$ free --mega -s 1
```

- `-s 1` - mostra as infos a cada 1 segundo

## grep

É usado para pesquisa dentro de arquivos. Por padrão é case-sensitive.

```sh
user@linux-admin:~$ grep
user@linux-admin:~$ grep 'root' /etc/passwd
user@linux-admin:~$ grep -v
user@linux-admin:~$ grep -f /tmp/expressao /etc/passwd
user@linux-admin:~$ grep -i
user@linux-admin:~$ grep -iE '^www-data*nologin$' /etc/passwd
user@linux-admin:~$ grep -iF '.*' /tmp/expressao
user@linux-admin:~$ grep -r
user@linux-admin:~$ grep -ri 'Foca02' .
user@linux-admin:~$ grep -ril 'foca02' .
user@linux-admin:~$ grep -rin 'foca02' .
```

- `grep ` - pesquisa dentro de arquivos
- `grep 'root' /etc/passwd` - exemplo de uso
- `grep -v` - inverte a busca, no que não tem.
- `grep -f /tmp/expressao /etc/passwd` - especifica um arquivo
- `grep -i` - ignora maiúsculas e minúsculas
- `grep -iE '^www-data*nologin$' /etc/passwd` - com expressão regular
- `grep -iF '.*' /tmp/expressao` - busca caracteres especiais
- `grep -r` - pesquisa de forma recursiva
- `grep -ri 'Foca02' .` - mostra o nome do arquivo e o texto buscado
- `grep -ril 'foca02' .` - mostra apenas o nome do arquivo
- `grep -rin 'foca02' .` - mostra o número da linha que tem a expressão

## head

```sh
user@linux-admin:~$ head
user@linux-admin:~$ head /etc/passwd
user@linux-admin:~$ head -n 3 /etc/passwd
user@linux-admin:~$ cat /etc/passwd | head -n 3
user@linux-admin:~$ head -c 10 /etc/passwd
```

- `head` - Mostra as primeiras linhas de um arquivo. Por padrão exibe as 10 primeiras.
- `head /etc/passwd` -
- `head -n 3 /etc/passwd` - apenas as 3 primeiras
- `cat /etc/passwd | head -n 3` - posso usar entrada padrão também.
- `head -c 10 /etc/passwd` - mostra os 10 primeiros bytes do arquivo

## nl

```sh
user@linux-admin:~$ nl /etc/passwd
user@linux-admin:~$ nl -f a /etc/passwd
user@linux-admin:~$ nl -f a -i 2 /etc/passwd
user@linux-admin:~$ nl -f a -i 3 -v 2 /etc/passwd
```

- `nl /etc/passwd` - enumera um determinado arquivo
- `nl -f a /etc/passwd` -
- `nl -f a -i 2 /etc/passwd` - incremente 2 linhas
- `nl -f a -i 3 -v 2 /etc/passwd` - Quando linha inicial. Começa a enumerar por 2 a linha.

# Comandos diversos 2

## more

Se temos um arquivo muito grande e não conseguimos visualizá-lo todo na tela, usamos o comando `more`.

```sh
user@linux-admin:~$ cat /etc/ssh/sshd_config
user@linux-admin:~$ cat /etc/ssh/sshd_config -n
user@linux-admin:~$ more /etc/ssh/sshd_config
user@linux-admin:~$ cat /etc/ssh/sshd_config | more
```

## less

Tem mais funcionalidades que o comando `more`.
É possível rolar a tela para cima ou para baixo utilizando apenas as setas.

```sh
user@linux-admin:~$ less /etc/ssh/sshd_config
```

Tem a função de pesquisa dentro do arquivo. Basta digitar `/` para entrar o termo de busca.

- `n` - para ocorrência seguinte.
- `q` - para sair.

## sort

```sh
user@linux-admin:~$ vi lista.txt
user@linux-admin:~$ sort lista.txt
user@linux-admin:~$ cat lista.txt | sort
user@linux-admin:~$ cat lista.txt | sort -r
user@linux-admin:~$ cat lista.txt | sort -n
user@linux-admin:~$ sort lista.txt -c
user@linux-admin:~$ sort lista.txt | sort -c
user@linux-admin:~$ sort lista.txt | sort -n -c
user@linux-admin:~$ sort -n lista.txt | sort -n -c
user@linux-admin:~$ sort -n lista.txt | sort -n -C
user@linux-admin:~$ vi listaNova.txt
user@linux-admin:~$ cat listaNova.txt | sort
user@linux-admin:~$ cat listaNova.txt | sort +1
user@linux-admin:~$ cat listaNova.txt | sort +1 -t " "
user@linux-admin:~$ cat listaNova.txt | sort -k 1
user@linux-admin:~$ cat listaNova.txt | sort -k 2
user@linux-admin:~$ cat /etc/passwd | sort -t ":" -k 1
user@linux-admin:~$ cat /etc/passwd | sort -t ":" -k 2
user@linux-admin:~$ cat /etc/passwd | sort -t ":" -k 3 -n
user@linux-admin:~$ cat /etc/passwd | sort -t ":" +2 -3 -n
```

- `sort +` - inicia em zero ou `sort -k`
- `/etc/passwd` - é onde tem o banco de dados do linux
- `vi lista.txt` - com algumas infos
- `sort lista.txt` - ordena os arquivos por números e depois letras.
- `cat lista.txt | sort` -
- `cat lista.txt | sort -r` - reverte a ordenação
- `cat lista.txt | sort -n` - ordena por números. Checar apenas se estiver ordenado.
- `sort lista.txt -c` -
- `sort lista.txt | sort -c` -
- `sort lista.txt | sort -n -c` -
- `sort -n lista.txt | sort -n -c` -
- `sort -n lista.txt | sort -n -C` -
- `vi listaNova.txt` -
- `cat listaNova.txt | sort` - ordena pela `primeira` coluna
- `cat listaNova.txt | sort +1` - ordena pela `segunda` coluna
- `cat listaNova.txt | sort +1 -t " "` - especifica o delimitador `" "`
- `cat listaNova.txt | sort -k 1` - primeiro campo como delimitador
- `cat listaNova.txt | sort -k 2` - segundo campo como delimitador
- `cat /etc/passwd | sort -t ":" -k 1` - classifica pela primeira coluna
- `cat /etc/passwd | sort -t ":" -k 2` - classifica pela segunda coluna
- `cat /etc/passwd | sort -t ":" -k 3 -n` - como terceira colunaé o id do usuário, classifica por ordem crescente.
- `cat /etc/passwd | sort -t ":" +2 -3 -n` - quando houver empate na classificação, pega a próxima coluna para desempate.

## tail

Comando `tail` permite visualizar o final de um arquivo no sistema operacional.

```sh
user@linux-admin:~$ tail /etc/passwd
user@linux-admin:~$ tail -n 4 /etc/passwd
user@linux-admin:~$ cat /etc/passwd | sort -t ":" +2 -n | tail -n 1
user@linux-admin:~$ cat /etc/passwd | sort -t ":" +2 -n | head -n 1
user@linux-admin:~$ tail -f /var/log/auth.log
```

- `tail /etc/passwd` - mostra as 10 últimas linhas do arquivo
- `tail -n 4 /etc/passwd` - mostra as 4 últimas linhas do arquivo
- `cat /etc/passwd | sort -t ":" +2 -n | tail -n 1` - mostra o usuário que tem o maior ID no sistema.
- `cat /etc/passwd | sort -t ":" +2 -n | head -n 1` - mostra o menor usuário qu tem o ID no sistema.
- `tail -f /var/log/auth.log` - Fica observando o arquivo. Interessante para fazer auditoria no sistema.

## time

Mostra o tempo de execução de um comando.

```sh
user@linux-admin:~$ time ls
user@linux-admin:~$ time find / -name giropops
```

## touch

Serve para criar um arquivo vazio.

```sh
user@linux-admin:~$ touch /tmp/arquivo
user@linux-admin:~$ ls -a /tmp/arquivo
user@linux-admin:~$ touch -t 10120815 /tmp/arquivo
user@linux-admin:~$ touch -a -t 10120815 /tmp/arquivo
```

- `touch /tmp/arquivo` - cria um arquivo vazio
- `ls -a /tmp/arquivo` -
- `touch -t 10120815 /tmp/arquivo` - altera a data do arquivo
- `touch -a -t 10120815 /tmp/arquivo` - altera a data de acesso

## uptime

Mostra o tempo que a máquina está ligada.

```sh
user@linux-admin:~$ uptime
```

## dmesg

Exibe as mensagens do ring buffer do kernel. É uma área que é utilizada para mostrar as últimas mensagens que foram exibidas no contexto do kernel. Por padrão exibe 64kb.

```sh
user@linux-admin:~$ dmesg -t
user@linux-admin:~$ dmesg -t | grep enp0S3
user@linux-admin:~$ dmesg --color=never
user@linux-admin:~$ dmesg -w
user@linux-admin:~$ dmesg -x
user@linux-admin:~$ dmesg -T
user@linux-admin:~$ dmesg -c
```

- `dmesg -t` - não exibe a coluna dos números. (1ª coluna)
- `dmesg -t | grep enp0S3` - busca pela placa de rede
- `dmesg --color=never` - nao mostra a cor
- `dmesg -w` - segue as mensagens do kernel
- `dmesg -x` - decodifica as mensagens para ser mais legível
- `dmesg -T` - pega o timestamp e converte para formato de data
- `dmesg -c` - limpa as mensagens do kernel.

## mesg

Hoje não é tão usado. Nos primórdios do Linux/Unix sim.

## talk

Usado para enviar mensagens em tempo real, com o comando `mesg`, você poderia aceitar ou não a mensagem do `talk`.

```sh
user@linux-admin:~$ mesg y
```

- `mesg y` - ativa o modo de recebimento.

## Comandos diversos 3

## su

O comando `su` permite elevar os privilégios do usuário comum para `usuário root`. A diferença do `su` para o `sudo` é que o `sudo` pede a senha do usuário, não precisa ter conhecimento da senha de `root`.

O comando `su` usa quando você realmente conhece a senha de `root`.

Com o comando `su` eleva ao usuário `root`, e limpa as variáveis de ambiente e inicia um ambiente completamente novo. É mais seguro.

Recomenda colocar sempre o path completo de acesso ao comando `su`.

```sh
user@linux-admin:~$ /bin/su
```

- força a execução do comando correto.

```sh
user@linux-admin:~$ cat /etc/passwd
user@linux-admin:~$ su - backup
user@linux-admin:~$ su - backup -s /bin/bash
```

- `cat /etc/passwd` -
- `su - backup` -
- `su - backup -s /bin/bash` - para especificar um shell para o usuário

## Instalar o sudo se não estiver disponível

```sh
root@linux-admin:~# apt update
root@linux-admin:~# apt-get install sudo
```

O comando `sudo` permite escalar privilégios para super usuário, e a vantagem é que pode especificar quais usuários poderão escalar privilégios para `root`, adicionando eles ao `grupo sudo`. Quando usado o comando `sudo` ele pedirá a senha do usuário comum, e não a senha de `root`.

```sh
user@linux-admin:~$ sudo id
user@linux-admin:~$ id
```

- `sudo id` - pedi a senha do usuário
- `id` - mostra os grupos que este user tem acesso.

Se quiser ficar como `root` permanente, digite `sudo su`, vai abrir um novo shell de root, pois já estou autenticado.

### Adicionando um usuário ao grupo `sudo`

```sh
root@linux-admin:~# adduser fabio sudo
root@linux-admin:~# deluser fabio sudo
root@linux-admin:~# groups
```

Obs: Para que as alterações tenham efeito, é necessário fazer o logout.

## sync

Serve para gravarmos os buffers do kernel em disco. Com `sync` forçamos a gravação imediata, sem a necessidade de espera de 10 a 20 segundos do padrão.

```sh
user@linux-admin:~$ sync
```

## Uname

Retorna o nome do nosso sistema operacional.

```sh
user@linux-admin:~$ uname
user@linux-admin:~$ uname -a
user@linux-admin:~$ uname -s
user@linux-admin:~$ uname -n
user@linux-admin:~$ uname -r
user@linux-admin:~$ uname -v
user@linux-admin:~$ uname -m
```

- `uname` -
- `uname -a` - retorna mais infos
- `uname -s` - mostra o nome
- `uname -n` - mostra o nome da máquina na rede O mesmo que `hostname`
- `uname -r` - retorna a versão do kernel.
- `uname -v` - mostra a data que foi compilado o kernel.
- `uname -m` - mostra a arquitetura desse kernel.

## Reboot

Somente o usuário `root` tem permissão apra reiniciar

```sh
user@linux-admin:~$ systemctl reboot
user@linux-admin:~$ shutdown -r now
user@linux-admin:~$ echo b >/proc/sysrq-trigger
user@linux-admin:~$ halt
user@linux-admin:~$ systemctl halt
user@linux-admin:~$ shutdown -h now
user@linux-admin:~$ echo o >/proc/sysrq-trigger
user@linux-admin:~$ shutdown -h 09:40
```

- `systemctl reboot` - se for fazer reboot forçado, usar o `sync` primeiro e depois o `reboot -f`
- `shutdown -r now` -
- `echo b >/proc/sys` - forma emergencial de reboot a máquina
- `halt` - para desligar a máquina
- `systemctl halt` -
- `shutdown -h now` -
- `echo o >/proc/sys` - forma emergencial de desligar a máquina.
- `shutdown -h 09:40` - agendamento de desligamento.

## wc

Conta o número de palavras, linhas, bytes em um arquivo.

```sh
user@linux-admin:~$ wc /etc/passwd
26 41 1407 /tc/passwd
user@linux-admin:~$ ls -l /etc/passwd
user@linux-admin:~$ cat /etc/passwd -n
user@linux-admin:~$ wc -l /etc/passwd
user@linux-admin:~$ wc -c /etc/passwd
user@linux-admin:~$ wc -w /etc/passwd
```

- `wc /etc/passwd` - 26=números de linhas, 41=palavras encontradas, 1407=tamanho
- `ls -l /etc/passwd` -
- `cat /etc/passwd -n` -
- `wc -l /etc/passwd` - somente número de linhas
- `wc -c /etc/passwd` - números de bytes
- `wc -w /etc/passwd` - números de palavras

## seq

Imprime uma sequência de números.

```sh
user@linux-admin:~$ seq 10
user@linux-admin:~$ seq 2 10
user@linux-admin:~$ seq 2 2 10
user@linux-admin:~$ seq -w 15
```

- `seq 10` - imprime uma sequência
- `seq 2 10` - vai numera a partir de 2
- `seq 2 2 1` - vai iniciar em 2, vai incrementar de 2 em 2
- `seq -w 15` - coloca zero a esquerda, para que a coluna fique visualmente melhor.
