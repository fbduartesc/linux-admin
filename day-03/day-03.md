# Curso Linux Admin

## Day 3

## Comandos internos e externos

Um comando interno é um comando que está embutido dentro do interpretador de comandos do próprio shell.

```sh
user@linux-admin:~$ echo $SHELL
/bin/bash
```

Shell script são automações feitas usando comandos do próprio sistema operacional.

```sh
user@linux-admin:~$ echo "Teste"
Teste
```

- `echo` usado para escrever
- `alias` para criar alias de comandos

```sh
user@linux-admin:~$ alias ls='ls -l'
user@linux-admin:~$ ls
total 0
```

Como faço para identificar se um comando é interno ou externo? Um comando interno é executado muito mais rápido que um comando externo. Com isso ganha mais performance na execução de aplicativos.

A 1ª forma é usando o comando `which` que identifica se o binário está instalado na sua máquina.

A 2ª forma vendo a própria página do manual do bash. `man builtins`

```sh
user@linux-admin:~$ man builtins
BASH-BUILTINS(7)                                      Miscellaneous Information Manual                                      BASH-BUILTINS(7)

NAME
       bash-builtins - bash built-in commands, see bash(1)

SYNOPSIS
       bash  defines  the  following built-in commands: :, ., [, alias, bg, bind, break, builtin, case, cd, command, compgen, complete, con‐
       tinue, declare, dirs, disown, echo, enable, eval, exec, exit, export, fc, fg, getopts, hash, help, history, if, jobs, kill, let,  lo‐
       cal,  logout, popd, printf, pushd, pwd, read, readonly, return, set, shift, shopt, source, suspend, test, times, trap, type, typeset,
       ulimit, umask, unalias, unset, until, wait, while.

```

Em `SYNOPSIS` temos os comandos que estão contidos dentro do nosso interpretador `bash.`

Esta é uma outra forma bastante utilizada. Outro interpretadores de comando como o `sh`, podem ter comandos internos diferentes do `bash.`

Por isso que em alguns `shell scripts`, você precisa especificar no início do arquivo qual o interpretador que será utilizado.

```sh
user@linux-admin:~$ less /etc/init.d/ssh
```

O exemplo acima, usa o padrão `POSIX.`

Se usar o `/bin/sh` torna o seu script mais portável entre plataformas.

Existem comandos que existem tanto internamente como externamente, por que eles não existem em outros interpretadores.
Um exemplo é o comando `echo.`

```sh
user@linux-admin:~$ echo "teste"
teste
user@linux-admin:~$ which echo
/bin/echo
user@linux-admin:~$ man builtins
```

Como faço para saber qual está sendo executado?

1º busca o comando internamente, por que é mais rápido a execução e mais seguro.
Por que mais seguro? Vou criar um script como rrot para mostrar.

Em `/usr/local/bin` ficam os binários adicionais, tudo que não vem na distrituição vem pra cá.

```sh
root@linux-admin:~# cd /usr/local/bin
root@linux-admin:~/usr/local/bin# vi alias
#! /bin/sh
echo "Nao execute este script"
```

Volte para o usuário normal e execute o comando `alias` criado.

```sh
user@linux-admin:~$ alias
```

A execução deste comando traz o `alias` que é embutido no shell.
Será que posso executar o `alias` que criei?

```sh
user@linux-admin:~$ echo $PATH
```

Indica o caminho que a máquina vai pesquisar para executar um comando do sistema.

Para executar o script criado.

```sh
user@linux-admin:~$ /usr/local/bin/alias
Nao execute este script
```

```sh
user@linux-admin:~$ type alias
```

Mostra se o comando esta sendo executa interno ou externamente.

## Manipulação de diretórios

```sh
user@linux-admin:/tmp/LinuxTips$ touch teste
```

Para criar aquivos

```sh
user@linux-admin:/tmp/LinuxTips$ ls -A
aaa  bbb  ccc  ddd  ltest  Pasta1  Pasta2  teste  teste~
```

Vai listar apenas os arquivos/diretórios normais e ocultos.

```sh
user@linux-admin:/tmp/LinuxTips$ touch teste~
user@linux-admin:/tmp/LinuxTips$ ls -B
aaa  bbb  ccc  ddd  ltest  Pasta1  Pasta2  teste
```

Não lista os arquivos com final `~` que são arquivos de backup no Linux. É uma convenção criar arquivos desta forma para bkp.

```sh
user@linux-admin:/tmp/LinuxTips$ mkdir Pasta1
user@linux-admin:/tmp/LinuxTips$ mkdir Pasta2
```

```sh
user@linux-admin:/tmp/LinuxTips$ ls --color=auto
user@linux-admin:/tmp/LinuxTips$ ls --color=always
user@linux-admin:/tmp/LinuxTips$ ls --color=never
```

Só usa a cor se o terminal tem suporte a cores. O Debian por padrão não traz as cores, por que é feito para rodar em qualquer tipo de ambiente.

```sh
user@linux-admin:/tmp/LinuxTips$ ls Pasta1 Pasta2
Pasta1:
arq1_pasta1  arq2_pasta1

Pasta2:
arq1_pasta2  arq2_pasta2
```

Se quisermos listar paenas o nome da pasta e não o conteúdo.

```sh
user@linux-admin:/tmp/LinuxTips$ ls -d Pasta1
```

```sh
user@linux-admin:/tmp/LinuxTips$ ls -l
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 aaa
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 bbb
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 ccc
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 ddd
lrwxrwxrwx 1 fabio fabio    5 ago 28 23:22 ltest -> teste
drwxr-xr-x 2 fabio fabio 4096 ago 28 23:06 Pasta1
drwxr-xr-x 2 fabio fabio 4096 ago 28 23:06 Pasta2
-rwxr-xr-x 1 fabio fabio    0 ago 28 22:56 teste
-rw-r--r-- 1 fabio fabio    0 ago 28 22:56 teste~
```

- `-rw-r--r-- 1` permissões
- `fabio` dono, quem criou
- `fabio` grupo que pertence
- `4096` tamanho
- `ago 28 22:56` data de criação ou modificação
- `Pasta1` nome da pasta ou arquivo

```sh
user@linux-admin:/tmp/LinuxTips$ ls -l Pasta1
-rw-r--r-- 1 fabio fabio 0 ago 28 23:06 arq1_pasta1
-rw-r--r-- 1 fabio fabio 0 ago 28 23:06 arq2_pasta1
```

Mostra todo o conteúdo

```sh
user@linux-admin:/tmp/LinuxTips$ ls -l Pasta1 -d
drwxr-xr-x 2 fabio fabio 4096 ago 28 23:06 Pasta1
```

Mostra apenas as informações dessa pasta.

```sh
user@linux-admin:/tmp/LinuxTips$ ls -f
teste  ddd  ..  ltest  ccc  Pasta2  Pasta1  teste~  bbb  aaa  .
```

Não classifica a listagem. Classifica por ordem de criação.

```sh
user@linux-admin:/tmp/LinuxTips$ ls -F
aaa  bbb  ccc  ddd  ltest@  Pasta1/  Pasta2/  teste*  teste~
```

Coloca um separador entre os arquivos.

- diretório - coloca `/`
- socket de rede - coloca `=`
- link simbólico - coloca `@`
- executável - coloca `*`

```sh
user@linux-admin:/tmp/LinuxTips$ ln -s teste ltest
user@linux-admin:/tmp/LinuxTips$ ls -F
user@linux-admin:/tmp/LinuxTips$ chmod 755 teste
```

`chmod` dando permissão de execução

```sh
user@linux-admin:/tmp/LinuxTips$ ls -l -G
user@linux-admin:/tmp/LinuxTips$ ls -lG
-rw-r--r-- 1 fabio    0 ago 28 23:23 aaa
-rw-r--r-- 1 fabio    0 ago 28 23:23 bbb
-rw-r--r-- 1 fabio    0 ago 28 23:23 ccc
-rw-r--r-- 1 fabio    0 ago 28 23:23 ddd
lrwxrwxrwx 1 fabio    5 ago 28 23:22 ltest -> teste
drwxr-xr-x 2 fabio 4096 ago 28 23:06 Pasta1
drwxr-xr-x 2 fabio 4096 ago 28 23:06 Pasta2
-rwxr-xr-x 1 fabio    0 ago 28 22:56 teste
-rw-r--r-- 1 fabio    0 ago 28 22:56 teste~
```

Oculta a coluna do grupo do arquivo.

```sh
user@linux-admin:/tmp/LinuxTips$ ls -ln
-rw-r--r-- 1 1000 1000    0 ago 28 23:23 aaa
-rw-r--r-- 1 1000 1000    0 ago 28 23:23 bbb
-rw-r--r-- 1 1000 1000    0 ago 28 23:23 ccc
-rw-r--r-- 1 1000 1000    0 ago 28 23:23 ddd
lrwxrwxrwx 1 1000 1000    5 ago 28 23:22 ltest -> teste
drwxr-xr-x 2 1000 1000 4096 ago 28 23:06 Pasta1
drwxr-xr-x 2 1000 1000 4096 ago 28 23:06 Pasta2
-rwxr-xr-x 1 1000 1000    0 ago 28 22:56 teste
-rw-r--r-- 1 1000 1000    0 ago 28 22:56 teste~
```

Pega o nome do dono e grupo e converte para `uid` e `gid` do Unix. Lista em formato numérico.

```sh
user@linux-admin:/tmp/LinuxTips$ ls -l -L
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 aaa
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 bbb
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 ccc
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 ddd
-rwxr-xr-x 1 fabio fabio    0 ago 28 22:56 ltest
drwxr-xr-x 2 fabio fabio 4096 ago 28 23:06 Pasta1
drwxr-xr-x 2 fabio fabio 4096 ago 28 23:06 Pasta2
-rwxr-xr-x 1 fabio fabio    0 ago 28 22:56 teste
-rw-r--r-- 1 fabio fabio    0 ago 28 22:56 teste~
```

Oculta os links simbólicos

```sh
user@linux-admin:/tmp/LinuxTips$ ls -l -o
-rw-r--r-- 1 fabio    0 ago 28 23:23 aaa
-rw-r--r-- 1 fabio    0 ago 28 23:23 bbb
-rw-r--r-- 1 fabio    0 ago 28 23:23 ccc
-rw-r--r-- 1 fabio    0 ago 28 23:23 ddd
lrwxrwxrwx 1 fabio    5 ago 28 23:22 ltest -> teste
drwxr-xr-x 2 fabio 4096 ago 28 23:06 Pasta1
drwxr-xr-x 2 fabio 4096 ago 28 23:06 Pasta2
-rwxr-xr-x 1 fabio    0 ago 28 22:56 teste
-rw-r--r-- 1 fabio    0 ago 28 22:56 teste~
```

Exibe apenas a coluna do dono

```sh
user@linux-admin:/tmp/LinuxTips$ ls -l -g
-rw-r--r-- 1 fabio    0 ago 28 23:23 aaa
-rw-r--r-- 1 fabio    0 ago 28 23:23 bbb
-rw-r--r-- 1 fabio    0 ago 28 23:23 ccc
-rw-r--r-- 1 fabio    0 ago 28 23:23 ddd
lrwxrwxrwx 1 fabio    5 ago 28 23:22 ltest -> teste
drwxr-xr-x 2 fabio 4096 ago 28 23:06 Pasta1
drwxr-xr-x 2 fabio 4096 ago 28 23:06 Pasta2
-rwxr-xr-x 1 fabio    0 ago 28 22:56 teste
-rw-r--r-- 1 fabio    0 ago 28 22:56 teste~
```

Exibe apenas a coluna do grupo

```sh
user@linux-admin:/tmp/LinuxTips$ ls -l -p
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 aaa
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 bbb
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 ccc
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 ddd
lrwxrwxrwx 1 fabio fabio    5 ago 28 23:22 ltest -> teste
drwxr-xr-x 2 fabio fabio 4096 ago 28 23:06 Pasta1/
drwxr-xr-x 2 fabio fabio 4096 ago 28 23:06 Pasta2/
-rwxr-xr-x 1 fabio fabio    0 ago 28 22:56 teste
-rw-r--r-- 1 fabio fabio    0 ago 28 22:56 teste~
```

Não exibe os arquivos executáveis, os `*` e `@`.

```sh
user@linux-admin:/tmp/LinuxTips$ ls -l -t
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 ddd
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 ccc
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 bbb
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 aaa
lrwxrwxrwx 1 fabio fabio    5 ago 28 23:22 ltest -> teste
drwxr-xr-x 2 fabio fabio 4096 ago 28 23:06 Pasta2
drwxr-xr-x 2 fabio fabio 4096 ago 28 23:06 Pasta1
-rwxr-xr-x 1 fabio fabio    0 ago 28 22:56 teste
-rw-r--r-- 1 fabio fabio    0 ago 28 22:56 teste~
```

Classifica por data, os mais recentes

```sh
user@linux-admin:/tmp/LinuxTips$ ls -l -tr
user@linux-admin:/tmp/LinuxTips$ ls -latr
-rw-r--r-- 1 fabio fabio    0 ago 28 22:56 teste~
-rwxr-xr-x 1 fabio fabio    0 ago 28 22:56 teste
drwxr-xr-x 2 fabio fabio 4096 ago 28 23:06 Pasta1
drwxr-xr-x 2 fabio fabio 4096 ago 28 23:06 Pasta2
lrwxrwxrwx 1 fabio fabio    5 ago 28 23:22 ltest -> teste
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 aaa
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 bbb
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 ccc
-rw-r--r-- 1 fabio fabio    0 ago 28 23:23 ddd
```

Classifica por data, de forma reversa.

### Bem interessante para localizar logs do sistema.

## Manipulação de diretórios

```sh
user@linux-admin:/tmp/LinuxTips$ ls -lac
```

Classifica pela data de alteração

```sh
user@linux-admin:/tmp/LinuxTips$ ls -laX
```

Classifica pela extensão

```sh
user@linux-admin:/tmp/LinuxTips$ ls -laR
```

Faz a listagem recursiva, lista todos os arquivos e diretórios de forma recursiva.

```sh
user@linux-admin:/tmp/LinuxTips$ mkdir pasta2/pasta2_1
user@linux-admin:/tmp/LinuxTips$ mkdir pasta2/pasta2_2
```

```sh
user@linux-admin:/tmp/LinuxTips$ ls -la
```

Listagem longa

```sh
user@linux-admin:/tmp/LinuxTips$ ls -F /bin
```

A `/bin` é feita de executáveis.

O comando `pwd` é bem prático no uso de automação de `shell script`, para salvar o diretório atual.

```sh
user@linux-admin:/tmp/LinuxTips$ mkdir dir1 dir2
```

Para criar mais de um diretório com um único comando.

```sh
user@linux-admin:/tmp/LinuxTips$ mkdir -p dir1/dir2/dir3
```

O comando `p` permite criar a estrutura de diretórios sem que a pasta exista.

```sh
user@linux-admin:/tmp/LinuxTips$ tree
├── aaa
├── bbb
├── ccc
├── ddd
├── Diretorio11
│   └── aaa
├── Diretorio12
│   └── Diretorio12_1
├── editor
├── editor-mcedit.txt
├── editor.txt
├── eee
├── giropops
├── ltest -> teste
├── Pasta1
│   ├── arq1_pasta1
│   └── arq2_pasta1
├── Pasta2
│   ├── arq1_pasta2
│   └── arq2_pasta2
├── teste
├── teste~
└── zzz -> aaa
```

Usa-se o comando `tree` para ver a árvore de diretórios de forma visual.
Para instalar basta executar os comandos.

```sh
root@linux-admin:~# apt-get update
root@linux-admin:~# apt-get install tree
user@linux-admin:/tmp/LinuxTips$ tree -A
```

O comando `rmdir` remove diretórios vazios. Se houver algum arquivo dentro, o diretório não será removido.

```sh
user@linux-admin:/tmp/LinuxTips$ rmdir dir1
user@linux-admin:/tmp/LinuxTips$ rmdir -p dir1
```

Irá remover toda a estrutura de diretórios, apenas de diretórios.

## Manipulação de arquivos

O comando `cat` é usado para visualizar o conteúdo de um arquivo.

```sh
user@linux-admin:/tmp/LinuxTips$ cat -n teste
```

Exibe o número da linha.

```sh
user@linux-admin:/tmp/LinuxTips$ cat -s teste
```

Oculta as linhas em brancos repetidas dentro de um arquivo.

```sh
user@linux-admin:/tmp/LinuxTips$ cat -b teste
```

Irá numerar apenas as linhas que possuem conteúdo.

```sh
user@linux-admin:/tmp/LinuxTips$ cat -E teste
```

Útil para desenvolvedor, fazendo parsing de arquivo, usado para saber onde está o final do arquivo. Adicione um `$` ao final de cada linha

```sh
user@linux-admin:/tmp/LinuxTips$ cat -T teste
```

Exibe o caracter `tab` como `^I`

Temos algumas variações para visualizar arquivos compactados:

- `zcat teste.gz` evita que tenha que descompactar o arquivo.
- `bzcat teste.bz2`
- `xzcat teste.xz`

```sh
user@linux-admin:/tmp/LinuxTips$ tac teste
```

Exibe o arquivo de forma invertida.

```sh
user@linux-admin:/tmp/LinuxTips$ tac -s n\ teste
```

Para saber que a string é usada para quebra de linha `\n`

```sh
user@linux-admin:/tmp/LinuxTips$ rm teste
```

Comando um pouco perigoso, tenha bastante cautela no uso. Remove arquivos ou diretórios de forma normal ou recursiva.

```sh
user@linux-admin:/tmp/LinuxTips$ rm -r dir1
```

Posso remover subdiretórios

```sh
user@linux-admin:/tmp/LinuxTips$ rm -rf dir1
```

Remove arquivo e diretório sem perguntar forçando a remoção.

```sh
user@linux-admin:/tmp/LinuxTips$ rm -i
```

Força pergunta de remoção, mas perde quando tem o `-f` junto.

```sh
user@linux-admin:/tmp/LinuxTips$ rm -f teste editor.txt
```

Posso especificar vários arquivos para remoção.

```sh
user@linux-admin:/tmp/LinuxTips$ rm -rf *
```

Remove todos os arquivos no diretório que não estão ocultos.

```sh
user@linux-admin:/tmp/LinuxTips$ rm -- -
```

Para remover arquivos que comecem com `-`. Em algumas distribuições Linux é obrigatório especificar este `--` para não ser confundido com um parâmetro.

O comando `cp` serve para copiar arquivos.

- `cp [origem] [destino]`
- `cp aaa eee`
- `cp /bin/ls giropops`

Se quiser copiar vários arquivos para um diretório:

- `cp aaa bbb ccc Diretorio1`
- `cp * diretorio` - copia para o diretório

Uso do modo `VERBOSE` para mostrar os arquivos que foram copiados. Não vem por padrão. Para usar sempre, cria-se um `alias.`

- `cp -v`
- `cp -R` - copia também dispositivos especiais e FIFOS. Se tiver copiando do `/dev` utiliza o `-R`.

A diferença entre o `-R` e o `-r` é que o `-r` ignora os dispositivos especiais.

- `cp -Rf * diretorio1/`
- `cp -vs aaa zzz` - Criação de link simbólico em vez de copiar os arquivos.
- `cp -vu * dirtorio1/` - Serve para copiar arquivos apenas se forem atualizados na origem. Se estiver utilizando arquivos grandes, ele irá comparar para ver se teve atualização do arquivo, senão faz a cópia. É uma forma mais inteligente de copiar os arquivos.
- `cp -x` - especifica se irá copiar os arquivos de outros sistema de arquivos.
- `cp -vrx * diretorio1`
- `cp -p` - Serve para preservar atributos do arquivo (dono, grupo e permissões) quando possível.
- `cp -a` - igual a opção `-d` para fazer referências aos links simbólicos. `cp -a = dpR`

O comando `mv` serve para mover arquivos para um destino. Após o processo de cópia a origem é apagada.

- `mv giropops destino`

Quando se esta dentro da mesma partição ele move apenas a referência do arquivo. O que não ocorre quando está em outra partição, onde faz a cópia e depois apaga da origem.
Ele pode ser usado para renomear o arquivo também.

Tem os mesmos parâmetros do comando `cp`.

- `mv -i eee aaa` - renomeado o arquivo
- `mv -f aaa destino` - Move sem perguntar. Tomar cuidado no uso.
- `mv destino/aaa .` - Move para o diretório atual.
- `mv -u`

## Editores de texto

O `nano` é o mais simples que acompanha as distribuições. Permite edição de forma simples.

- `nano editor.txt`

O `mcedit` é um editor que acompanha o pacote `mc.`

- `mcedit editor.txt` - abrir arquivo

```sh
root@linux-admin:~# apt-get install mc
```

O `vi` é o editor tradicional, editor modal, possui alguns modos de operação.Torna um pouco mais complexo para edição de arquivo. Exige um pouco mais de conhecimento da operação.

No Debian tem uma versão mais compacta do editor.

- `vi editor.txt`

```sh
user@linux-admin:/tmp/LinuxTips$ dpkg -l vim-tiny
```

Logo que abre o `editor vi` ele não permite edição de texto. Está no modo de comando. Para executar os comandos, digite `SHIFT + :`

- `q` sai do editor
- `q!` sai sem salvar
- `i` para iniciar a edição do texto
- `esc` para voltar modo de comando.
- `x` salva e sai
- `w` salva apenas o arqivo.
