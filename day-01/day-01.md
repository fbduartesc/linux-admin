# Curso Linux Admin

## Day 1

Objetivos do curso

- Conceitos Linux
- Gerenciamento de pacotes
- adicionar/remover programas
- configuração de serviços
- configuração modo gráfico
- Troubleshooting - solução de problemas

Terá base para fazer a certificação Linux LPI1 e LPI2

Instrutores:
- Gleyson Mazioli
- Jeferson

Criou o Guia Foca Linux => [guiafoca.org](https://guiafoca.org)

- Linux
- Distribuições Linux
- Licenças
- GNU e Software Livre
- BSD

Linux é um kernel, criado em 1991 pelo filandês Linus Torvalds. Ele alterou o minix.
Foi criado para ser um kernel modularizado, tem um núcleo que parte deles são carregados quando necessário.
Dá uma potencializada enorme de performance.
Comunidade - compartilhamento

O linux é só kernel da distribuição.
> 1993 - Debian - Ubuntu 2004.

> Red Hat - CentOS.

> Slacker 92 e 93.


# Licenças

Definem os direitos e responsabilidades do uso de ferramentas.

- Kernel do Linux está sobre GPL V2.
- Todas garantem uma **liberdade de distribuição**.
- Licença mais restritivas são as GPLs (GPL v2 e GPL v3)

> Restrição não é só impedir de utilizar, é impedir de você de realizar **algumas ações.**

GPLs exigem que você coloque os códigos fontes disponíveis, não a venda do software. Isso faz com que a licença seja **restritiva.**

As outras licenças são um pouco mais **permissivas.**
- GPL v2
- GPL v3
- BSD
- Apache
- MIT
- Creative Commons

# GNU e Software

**GNU** é um projeto - 1983 Richard Stallman

> Livre - refere-se a liberdade e não ao preço.

> Você tem 4 liberdade:
  - executar como quiser.
  - distribuir para quem você quiser.
  - estudar o software.
  - mudar e melhorar.

Sem as ferramentas **GNU**, não teríamos um Linux funcional. Uns chamam de GNU/Linux, por causa desse casamento de ferramentas.

# DFSG - Debian Free Software Guideline

- são regras para distribuição de software livre.
- redistribuição livre
- inclusão do código fonte
- permitindo modificações e trabalhos derivados
- integridade do código fonte do autor (como um compromisso)
- sem discriminação contra pessoas ou grupo
- sem discriminação contra campos de ativação, como uso comercial
- licença deve aplicar-se a todos aquele para quem o programa é redistribuído
- licença não deve ser específico para um produto
- licença não deve restringir outro software

# BSD - Berkeley Software Distribution

- é um sistema operacional **Unix** com desenvolvimento derivado e dsitribuído pelo Computer Systems Research Group da Universidade da Califórnia em Berkley, de 1977 a 1995.

# Instalação do Debian

> executar como root para instalar os pacotes.

```sh
root@linux-admin:~# apt install tree coreutils bsdutils bsdmainutils net-tools man-db
```

