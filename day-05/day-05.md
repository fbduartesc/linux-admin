# Curso Linux Admin

## Day 5

## Comandos diversos 04

## chattr

Permite alterar atributos do sistema de arquivos do sistema operacional Linux, e também diretórios. Trabalha em conjunto com o **_lsattr_** para listar e alterar atributos.

Atributos são diferentes de permissões. Estão relacionadas a segurança.

```sh
user@linux-admin:~$ mkdir /tmp/attr
user@linux-admin:~$ cd /tmp/attr
user@linux-admin:~$ ls -l
user@linux-admin:~$ touch teste
user@linux-admin:~$ touch teste1
user@linux-admin:~$ touch teste2
user@linux-admin:~$ ls -l
user@linux-admin:~$ lsattr
user@linux-admin:~$ chattr i
user@linux-admin:~$ chattr +i
user@linux-admin:~$ rm -f teste1
user@linux-admin:~$ rm -f teste
user@linux-admin:~$ chattr -i teste
```

- `lsattr` - vou listar os atributos que tem nesses arquivos
- `chattr i` - imutável, não permite alteração ou deleções em um arquivo ou diretório.
- `chattr +i` - usamos o `+` para dar um atributo a um arquivo.
- `rm -f teste` - não pode remover, mesmo estando como `root`.
- `chattr -i teste` - para remover o atributo.

É importante utilizar essa forma de bloqueio, quando existe algum arquivo que não possa ser alterado.
O arquivo fica blindado com o atributo `+i`.

```sh
user@linux-admin:~$ lsattr attr
```

- `lsattr attr` - lista os atributos de diretório.

> Se pegar o erro de `Operação não permitida`, pode verificar os atributos do sistema.

```sh
user@linux-admin:~$ chattr -R -i attr/
user@linux-admin:~$ chattr +a teste
user@linux-admin:~$ echo "Linha 1" >> teste
user@linux-admin:~$ echo "linha 2" >> teste
user@linux-admin:~$ echo "linha 3" >> teste
user@linux-admin:~$ cat teste
```

- `chattr -R -i attr/` - remover recursivamente.
- `chattr +a teste` - permite colocar um arquivo ou diretório no modo append.
- `echo "Linha 1" >> teste` - adiciona a mensagem ao final do arquivo.

O `>>` adiciona ao final do arquivo. Se tentar usar o `>` a operação será bloqueada, por que o arquivo está com o atributo para inserção apenas no final do arquivo.

Quando se tem o atributo de `append` não é possível trabalhar com a maioria dos editores, como o **VI**, **mcedit** (criam um arquivo temporário e sobrescrevem o arquivo original).

É usado para logs, para que não seja possível alterar seus conteúdos, apenas adicionar ao final.

```sh
user@linux-admin:~$ chattr +a attr
user@linux-admin:~$ chattr -d attr
```

Na pasta o atributo `+`, impede a remoção dos arquivos, mas permite a edição.

```sh
user@linux-admin:~$ mcedit -b teste
user@linux-admin:~$ ls -latr
user@linux-admin:~$ rm -f teste
```

Há a opção de ajudar os atributos de diretórios e arquivos.

```sh
user@linux-admin:~$ chattr =ai *
user@linux-admin:~$ chattr -i *
user@linux-admin:~$ chattr =aie *
user@linux-admin:~$ lsattr
user@linux-admin:~$ chattr +c teste
user@linux-admin:~$ chattr -c teste
user@linux-admin:~$ chattr +s teste
user@linux-admin:~$ chattr +S teste
user@linux-admin:~$ chattr +D attr
user@linux-admin:~$ lsattr -d attr
user@linux-admin:~$ chattr +d attr
user@linux-admin:~$ lsattr -d attr/
```

- `chattr =ai *` - vai atribuir a todos os arquivos o `ai`
- `chattr =aie *` - `e` extends
- `chattr +c teste` - faz com que o arquivo seja compactado.
- `chattr +s teste` - permite deleção segura, demora um pouco mais por que zera os blocos.
- `chattr +S teste` - permite fazer o sync do arquivo imediatamente salvamos o arquivo já é gravado em disco.
- `chattr +D attr` - qualquer arquivo que for gravado dentro do diretório, será gravado de forma sincrona.
- `chattr +d attr` - não sejam incluídos no backup **dump**, que é usado como padrão no Linux.

## cut

Permite que você corte um pedaço do arquivo para que exiba apenas uma parte do conteúdo.

```sh
user@linux-admin:~$ cat /etc/passwd
user@linux-admin:~$ cut -d ":" -f 1 /etc/passwd/
user@linux-admin:~$ cut -d ":" -f 1,7 /etc/passwd/
user@linux-admin:~$ cut -d ":" -f 1-3,7 /etc/passwd/
user@linux-admin:~$ cut -b 1-4 /etc/passwd
user@linux-admin:~$ cut -c 1-4 /etc/passwd
```

- `cut -d ":" -f 1 /etc/passwd/` - **d** delimitador, **-f** qual campo queremos pegar
- `cut -d ":" -f 1-3,7 /etc/passwd/` - com intervalos
- `cut -b 1-4 /etc/passwd` - números de bytes
- `cut -c 1-4 /etc/passwd` - **-c** conta por caracteres. Não conta os espaços, apenas caracteres válidos. Já o **-b** pega todos os caracteres, inclusive caracteres especiais.

## cmp

Serve para comparar um arquivo

```sh
user@linux-admin:~$ echo "oi" > teste
user@linux-admin:~$ cp teste teste-copia
user@linux-admin:~$ cmp teste teste-copia
user@linux-admin:~$ echo linha2 >> teste-copia
user@linux-admin:~$ cmp teste teste-copia
user@linux-admin:~$ cmp -s teste teste-copia
user@linux-admin:~$ echo $?
```

- `cmp -s teste teste-copia` - não retorna nenhuma saída. Retorna apenas o código de saída.
- `echo $?` - 1

No linux **0** quer dizer que foi executado com sucesso.

Se quiser retornar as linhas diferentes que criamos?

```sh
user@linux-admin:~$ diff teste teste-copia
user@linux-admin:~$ echo linha3 >> teste-copia
user@linux-admin:~$ diff tesste teste-copia
user@linux-admin:~$ linha3
user@linux-admin:~$ diff -u teste teste-copia
user@linux-admin:~$ diff -r
```

- `diff -u teste teste-copia` - retorna **mais** legível para humanos. Formato de comparação unificado.
- `diff -r` - permite comparar pastas.

```sh
user@linux-admin:~$ mkdir diff1 diff2
user@linux-admin:~$ cd diff1
user@linux-admin:~$ echo conteudo >> arquivo1
user@linux-admin:~$ echo conteudo >> arquivo2
user@linux-admin:~$ echo conteudo >> arquivo3
user@linux-admin:~$ cd ../diff2
user@linux-admin:~$ echo conteudo >> arquivo1
user@linux-admin:~$ echo conteudo >> arquivo2
user@linux-admin:~$ echo conteudo >> arquivo3
user@linux-admin:~$ echo conteudo2 >> arquivo1
user@linux-admin:~$ echo conteudo2 >> arquivo2
user@linux-admin:~$ cd ..
user@linux-admin:~$ diff -ru diff1 diff2
```

- `diff -ru diff1 diff2` - mostra as diferenças por **arquivo**.

Essa ferramenta, o **diff**, é utilizado para fazer alterações em código fonte de aplicativos. É o mesmo conceito utilizado pelo **git** para fazer cálculo de diferenlas, muito tradicional no **Unix**. inclusive para você aplicar alterações incrementais em código de sistemas.

```sh
user@linux-admin:~$ diff -ru diff1 diff2 > diferencas.patch
user@linux-admin:~$ cat diferencas.patch
user@linux-admin:~$ cd diff1
user@linux-admin:~$ cat arquivo1
user@linux-admin:~$ cat arquivo2
user@linux-admin:~$ cat arquivo3
```

- `diff -ru diff1 diff2 > diferencas.patch` - pegar a saída das diferenças e jogar no arquivo.

Se eu quiser mandar uma cópia atualizada do sistema, tenho **duas opções**:

- pegar o sistema todo, que pode ser muito grande, como é o caso do kernel do linux, ocupa 1GB.
- mandar apenas o que foi alterado entre uma versão e outra. Essa é uma técnica bastante utilizada no código fonte do kernel, para fazer a distribuição, você pode simplismente baixar os arquivos de diferenças e aplicar na sua máquina.

## Instalar o patch

```sh
user@linux-admin:~$ apt-get install patch
user@linux-admin:~$ patch -p1 -N </tmp/
user@linux-admin:~$ patch -p1 -N </tmlp/diferencas.patch>
```

- `-p1 -N </tmp/` - **p1** nível da pasta referente onde o patch foi aplicado. Se apliquei o meu diff gerido na pasta **/tmp/**, aqui entrei na pasta **diff1**, então tem que usar o **-p1**, para ele descer uma pasta para fazer a comparação do conteúdo. **-N** para não desfazer patches que já foram aplicados. **</tmp/diferencas.patch**, local do arquivo que tem as diferenças.

É muito comum falarmos que vamos fazer um patch no arquivo. Pegar as diferenças e aplicar as correções.

```sh
user@linux-admin:~$ cat arquivo1
user@linux-admin:~$ cat arquivo2
user@linux-admin:~$ cd ..
user@linux-admin:~$ diff -ru diff1 diff2
```

Posso desfazer o patch aplicado com o **-R** para reverter um patch aplicado.

```sh
user@linux-admin:~$ patch -p1 -R </tmp/diferencas.patch
```

Conceito base que é utilizado no git e outras ferramentas que fazer essa comparação de conteúdo, código fonte, para enteder o que foi alterado e salvar as diferenças sem ocupar muito espaço, porque comparado ao conteúdo, o arquivo de patch é bem pequeno.

```sh
user@linux-admin:~$ ls -l diferencas.patch
```

É mais usado em arquivos textos. O diff, se for para ver as diferenças com binários, usar o **cmp**.

```sh
user@linux-admin:~$ whereis
user@linux-admin:~$ whereis ls
user@linux-admin:~$ zless /usr/share/man/man1/ls.1.gz
user@linux-admin:~$
```

- `whereis` - permite localizar um arquivo que contém uma página de manual do sistema.
- `whereis ls` - vai mostrar qual arquivo contém a página de manual, que é uma documentação mais completa do **ls**
- `zless /usr/share/man/man1/ls.1.gz` - para abrir arquivos compactados **.gz**.

Está no formato de página de manual, que será aberto por meio do utilitário **man**.

```sh
user@linux-admin:~$ man ls
```

Possui um detalhamento mais dos comandos.

```sh
user@linux-admin:~$ which ls
user@linux-admin:~$ which mc
user@linux-admin:~$ which man
```

- `which ls` - permite localizar o binário no **S.O**

Se usa o **which** para localizar um binário de super usuário.

```sh
user@linux-admin:~$ which ifconfig
```

- `which ifconfig` - se tiver como usuário convencional, ele não irá exibir o caminho, por que não tenho acesso ao diretório **sbin**

```sh
user@linux-admin:~$ su -
user@linux-admin:~$ which ifconfig
```

- `which ifconfig` - agora mostra

Isso quer dizer que eu não posso executar o binário acima? Nem sempre.
Se eu estiver como usuário convencional e executar o comando passando o caminho completo, irá executar, por que este comando tem permissão para executar.

Nem todas as funções estão disponíveis.

Como usuário normal, não consigo alterar o **IP** da máquina, mas posso visualizar.
