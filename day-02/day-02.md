# Curso Linux Admin

## Day 2

Não teremos modo gráfico, é tela preta mesmo, modo texto.

## Primeiro acesso no Linux via SSH

```sh
user@linux-admin:~$ ssh <user-name>@<ip-name-machine>
```

Cat é um comando para visualizar o conteúdo de um arquivo.

```sh
user@linux-admin:~$ cat /etc/debian_version
11.0
```

- `user` - usuário
- `@` - at significa que o usuário tal está na máquina tal.
- `linux-admin` - nome ou ip da máquina
- `:` - separador
- `~` - símbolo bastante comun no linux, simbolizando seu diretório home
- `$` - quem está logado não é o user root com `uid = 0`. Usuário comum.
- `#` - identificação do root.

```sh
user@linux-admin:~$ pwd
/home/user
```

Quando está logado como root, você pode fazer tudo no seu linux.

```sh
user@linux-admin:~$ su -
```

Comando para ir para o super usuário, o root, não to passando nenhum parâmetro.

```sh
root@linux-admin:~# pwd
/root
```

O user root é o único que não tem o diretório dele dentro do `/home/`, todos os users estariam dentro do `/home/`.

- `pwd` - mostra o diretório corrente
- `cd` - troca de diretório

```sh
root@linux-admin:~# cd /home/user
```

## Conhecendo o Shell do Linux

- `cd -` vai para o meu último diretório.
- `cd ~` vai para o diretório do usuário.
- `ls` lista os diretórios.
- `ls -a` lista todos os arquivos, inclusive os ocultos.
- `ls -al` mais informações, quem é o dono, permissões
- `ls -alh` o `h` mostra o tamanho do arquivo legível para humanos.
- `-` é o parâmetro do comando
- `ls -lha`fica mais fácil de gravar esse comando.

**Limpar a tela** - `CTRL + L` atalho para o comando `clear`.

Está usando um shell para interagir com o Linux. O Linux não é tudo, é apenas o Kernel.

Temos um software chamado **Shell**, um componente. Ele tem vários sabores: o modelo do **Shell** que estamos utilizando é chamado de **Bash**.

Tem vários shells: `ksh, bash, zsh.`

Cada shell possui algumas caracteristicas. Em 99% dos Linux, vai encontrar o **Bash.**

O **bash** tem algumas caracteristicas:

- seta para cima, mostra os comandos digitados

- `history` - traz todo histórico de comandos que digitou.
- `cat /etc/shells` - lista todos os shells disponíveis.
- `dash` - não tem opção de seta para cima. Para sair digita `exit` ou `CTRL + D`.
- `cd ..` - volta para o diretório anterior.

```sh
root@linux-admin:~# ls -lha
drwxr-xr-x 2 user user 4,0K ago 23 20:19 .
drwxr-xr-x 3 root  root  4,0K ago 20 11:18 ..
-rw------- 1 user user  325 ago 27 14:15 .bash_history
-rw-r--r-- 1 user user  220 ago 20 11:18 .bash_logout
-rw-r--r-- 1 user user 3,5K ago 20 11:18 .bashrc
-rw-r--r-- 1 user user  807 ago 20 11:18 .profile
```

- `d` letra que aparece no inícia da listagem, diz que é um diretório. São referência.
- `.` é referência para o diretório atual, que está no momento.
- `..` é um diretório anterior.

## O que é um arquivo e o que é um diretório

- `diretório` - reúne vários arquivos e/ou diretórios
- `arquivo` - um arquivo de texto
- `cd /` - a barra é a raiz do sistema. É uma árvore invertida.

```sh
drwxr-xr-x 18 root root 4,0K ago 20 11:09 ..
-rw-------  1 root root 4,0K ago 27 21:53 .bash_history
```

- `d` - é um diretório
- `-` quando não tiver nenhuma letra, apenas o `hífen` quer dizer que é um arquivo comum.

**Caminho absoluto** - `cd /usr/lib/systemd/system`

**Caminho relativo** - `cd system`

Os diretórios no **/** são padrões no Linux.

## Estrutura de diretórios Linux e FHS

**LSB** => _Linux Standard Base_ -várias distribuições Linux se reuniram para definir algumas normativas para o sistema Linux. É uma padronização entre as distribuições.

**FHS** - _Filesystem Hierarchy Standard_ - define a estrutura de diretórios.

## /bin

Binários do sistema.
Tudo que é arquivo executável vai neste diretório.

```sh
root@linux-admin:~# cd /bin
root@linux-admin:/bin/#  ls | more
root@linux-admin:/bin/#  ls -lha | grep ls
lrwxrwxrwx  1 root root       8 nov  7  2019 dnsdomainname -> hostname
-rwxr-xr-x  1 root root     19K abr 29 04:11 dnstap-read
```

O comando `grep` serve para procurar. No comando acima ele pega a saída do `ls -lha` e vai procurar pela palavra lá dentro.

O `l` na lista de arquivos é um `link simbólico`. Ele é um atalho, e faz referência para um outro arquivo original.

## /boot

Arquivo do gerenciador de partido e Kernel, símbolos.

- `vmlinuz-5.10.0-8-amd64` - é o kernel do Linux.

## /dev

- devices - dispositivos
- no Linux tudo é um arquivo.

```sh
root@linux-admin:/bin/#  ls -lha | more
```

- Para paginar, vai apertando `barra de espaço` vai pulando tela ou `enter` para pular linha.

## Har disk

- `sda` - hd Sata
- `hda` - hd mais antigo.

O Linux divide o hd em 4 partições primárias e até 12 lógicas.

Primárias - `a` seria o primeiro HD físico na máquina, `b` o segundo e assim por diante.

> - sda
> - sda1
> - sda2
> - sda3

Lógicas

> - sda5
> - sda6
> - sda7
> - ...

Temos dois dispositivos:

- Bloco - dispositivos de armazenamento, hd, pendrive.
- caracter -dispositivo que está transferindo informação.

```sh
root@linux-admin:/dev#  ls -lha
lrwxrwxrwx   1 root  root           4 ago 27 21:54 rtc -> rtc0
brw-rw----   1 root  disk      8,   0 ago 27 21:54 sda
brw-rw----   1 root  disk      8,   1 ago 27 21:54 sda1
brw-rw----   1 root  disk      8,   2 ago 27 21:54 sda2
```

A saída acima podemos ver dispositivo de bloco e caracter.

## tty

É o terminal. Para acessar basta usar a combinação de teclas.
`CTRL + ALT + F[1,2,3,4,5,6] ou no Mac fn + CTRL + ALT + F[1,2,3,4,5,6]`

## psaux - Mouse

```sh
root@linux-admin:/dev#  cat < psaux
```

Mostra caracteres estranhos conforme move o mouse. Deve ter feito diretamente no computador, não é possível via ssh.

```sh
root@linux-admin:/dev#  df -h
```

Mostra as partições

## /etc

Arquivos de configurações globais, configuração da máquina.

```sh
root@linux-admin:/etc# cd /etc
```

Mostra o nome da máquina que é um arquivo de configuração.

```sh
root@linux-admin:/etc# cat hostname
linux-admin
```

Quando loga no Linux aparece uma mensagem, no caso a mensagem do dia.

```sh
root@linux-admin:/etc# cat motd
The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
```

Se quisermos podemos alterar essa mensagem.

```sh
root@linux-admin:/etc# echo 'nova mensagem do dia' > /etc/motd
root@linux-admin:/etc# cat motd
```

Quando subir um servidor web, como Apache, Nginx, vai esta dentro do /etc.

## /lib

- Bibliotecas essenciais do sistema, binários localizados em /bin e /sbin.
- Módulos do Kernel.
- Não é obrigatório o uso de extensão no Linux.
- módulos com extensão na pasta de drivers.
- As únicas exceções no Linux que tem extensões é nos Drivers e Bibiotecas.
- `.ko` módulos do kernel, que seriam os drivers.

## lost+found - achados e perdidos

- O sistema de arquivos do Linux ext3, ext4, como se fosse o NTFS do Windows.
- No Linux todo sistema de arquivos que possui `Journous`, é uma forma de recuperar seus arquivos.

## /media

- Faz o ponto de montagem dos dispositivos

## /mnt

- serve para monstagem também. Hoje em dia é um ponto de montagem temporário.

## /opt

- seria usado para instalação dos arquivos

## /proc

- é um diretório que existe mas não existe
- filesystem virtual, tem informação dinâmica do sistema e do processo

Informação sobre a CPU

```sh
root@linux-admin:/proc# cat cpuinfo
processor	: 0
vendor_id	: GenuineIntel
cpu family	: 6
model		: 23
model name	: Intel(R) Core(TM)2 Duo CPU     T8100  @ 2.10GHz
stepping	: 6
microcode	: 0x60c
cpu MHz		: 798.102
cache size	: 3072 KB
```

Informação sobre a memória

```sh
root@linux-admin:/proc# cat meminfo
MemTotal:        4005712 kB
MemFree:         3815656 kB
MemAvailable:    3747124 kB
Buffers:           10144 kB
Cached:            75480 kB
```

## PID (Process Identification)

- quando está rodando algum processo recebe um ID.

## /run

- traz informações de tudo que esta rodando no sistema, são dados dinâmicos.
- é considerado um arquivo temporário.

Se executarmos o comando:

```sh
root@linux-admin:/run# df -h
Sist. Arq.      Tam. Usado Disp. Uso% Montado em
udev            1,9G     0  1,9G   0% /dev
tmpfs           392M  860K  391M   1% /run
/dev/sda2       109G  1,1G  102G   2% /
tmpfs           2,0G     0  2,0G   0% /dev/shm
tmpfs           5,0M     0  5,0M   0% /run/lock
/dev/sda1       511M  3,5M  508M   1% /boot/efi
tmpfs           392M     0  392M   0% /run/user/1000
```

- Terá o `tmpfd` montado em `/run`

## /sbin

- binários de uso do super usuário (root)
- `swapoff`, `swapon` - liga/desliga a swap.
- `shutdown` - para dsligar a máquina, precisa ter permissão de root.

## /srv

- dados estáticos

## /sys

- relacionado aos devices

## /tmp

- temporário

## /usr

- hierarquia secundária, binários não essenciais para o sistema.
- quando vai compilar o kernel, o fonte deixa dentro do `src`

## /usr/local

- terceira hierarquia para dados locais.

## /var

- arquivos que são gravados com muita frequência
- `logs` do sistema

### /var/bin

- docker, volumes, database, vem tudo de lá.
- É importante por ter os logs do sistema.

# Desligando e reiniciando o Linux

Temos algumas opções:

- `halt` - desliga
- `reboot` - reinicia
- `shutdown -h now` - desliga a máquina agora
- `shutdown -r now` - reinicia a máquina agora
- `power off` - desligar
- `init 0` - desligar
- `init 6` - reiniciar
- `shutdown --help`
- `shutdown -k -h now` - simula que vai desligar e manda mensagem para todos
- `shutdown -h 18:00` programa desligamento
- `shutdown -h +30`
- `shutdown -c` - para cancelar
- `shutdown -r +30` "Vou reiniciar"
